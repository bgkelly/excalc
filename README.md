# ExCalc

Toy calculator app, just for fun.

#Install

Install latest [nodejs stable](https://nodejs.org/en/download/current/) for your platform.

Install gulp
```
$ npm i -g gulp
```

Change directories to the project folder, then install all dependencies
```
$ npm i
```

#Running

You can run the dev server via gulp:
```
$ gulp dev
```

#Build / Deployment

You can build the project with gulp:
```
$ gulp build
```

Deployment is optional and deploys to Surge.sh. Replace the deploy domain with your own.
```
$ gulp deploy
```