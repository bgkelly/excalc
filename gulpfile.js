var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    surge = require('gulp-surge');

gulp.task('js-watch', ['js'], function(cb) {
    browserSync.reload();
    cb();
});

gulp.task('css-watch', ['css'], function() {
    return gulp.src('app/css/styles.css')
        .pipe(browserSync.stream());
});

gulp.task('css', function() {
    return gulp.src(['app/styles/*.css', '!app/styles/*.min.css'])
        .pipe(sourcemaps.init())
        .pipe(postcss([require('postcss-cssnext'), require('postcss-import'), require('cssnano')]))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/styles/'));
});

gulp.task('js', function() {
    return gulp.src(['app/js/*.js', '!app/js/*.min.js'])
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/js/'));
});

gulp.task('build', ['css', 'js']);

gulp.task('dev', ['build'], function() {
    browserSync.init({
        server: {
            baseDir: './app'
        }
    });

    gulp.watch('app/*.html').on('change', browserSync.reload);
    gulp.watch('app/js/*.js', ['js-watch']);
    gulp.watch('app/styles/*.css', ['css-watch']);
});

gulp.task('deploy', ['build'], function() {
    return surge({
        project: './app',
        domain: 'excalc.surge.sh'
    });
});