(function() {
    'use strict';

    var Meta = {
        currentFormula: ' ',
        currentInput: '0',
        showingResult: false
    };

    var updateDisplay = function(){
        var display = document.querySelector('.calc__display');

        display.innerHTML = Meta.currentInput;
    };

    var updateFormulaDisplay = function() {
        var formulaDisplay = document.querySelector('.calc__formula');
        formulaDisplay.innerHTML = Meta.currentFormula.replace(' ', '&nbsp;');
    };

    var updateFormula = function(input, operand) {
        Meta.currentFormula += input + ' ' + operand + ' ';
        Meta.currentInput = '0';

        updateDisplay();
        updateFormulaDisplay();
    };

    var valueClick = function() {
        if(Meta.currentInput === '0'){
            Meta.currentInput = '';
        }

        if(Meta.showingResult){
            Meta.currentInput = '';
            Meta.showingResult = false;
        }

        Meta.currentInput += this.dataset.value;
        updateDisplay();
    };

    var operandClick = function() {
        Meta.showingResult = false;
        updateFormula(Meta.currentInput, this.dataset.operand);
    };

    var modifierClick = function() {
        Meta.showingResult = false;

        var modifier = this.dataset.modifier;

        if(modifier === 'c') {
            Meta.currentFormula = ' ';
            updateFormulaDisplay();
            Meta.currentInput = '0';
            updateDisplay();
        }
        else if(modifier === 'ce') {
            Meta.currentInput = '0';
            updateDisplay();
        }
        else if(modifier === 'del') {
            Meta.currentInput = Meta.currentInput.length > 1 ? Meta.currentInput.slice(0, -1) : '0';
            updateDisplay();
        }
        else if(modifier === '-' && Meta.currentInput !== '0') {
            Meta.currentInput = Meta.currentInput.indexOf('-') === 0 ? Meta.currentInput.slice(1) : '-' + Meta.currentInput;
            updateDisplay();
        }
        else if(modifier === '.' && Meta.currentInput.indexOf('.') === -1) {
            Meta.currentInput += '.';
            updateDisplay();
        }

    };

    var compute = function() {
        updateFormula(Meta.currentInput, '');
        var computedValue = eval(Meta.currentFormula);
        Meta.currentInput = '' + computedValue;
        Meta.currentFormula = ' ';

        updateDisplay();
        updateFormulaDisplay();
        
        Meta.showingResult = true;
    };

    var initButtons = function() {
        var values = document.querySelectorAll('[data-value]'),
            operands = document.querySelectorAll('[data-operand]'),
            modifiers = document.querySelectorAll('[data-modifier]'),
            computeEl = document.querySelector('.calc__button--compute');

        [].forEach.call(values, function(el) {
            el.addEventListener('click', valueClick);
        });

        [].forEach.call(operands, function(el) {
            el.addEventListener('click', operandClick);
        });

        [].forEach.call(modifiers, function(el) {
            el.addEventListener('click', modifierClick);
        });

        computeEl.addEventListener('click', compute);
    };

    initButtons();
}());